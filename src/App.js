import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from './pages/Login';
import 'bootstrap/dist/css/bootstrap.min.css';
import Insta from "./pages/Insta";
import React from 'react'
import Drafts from "./pages/Drafts";
import Instructor from "./pages/Instructor";

function App() {
  return (
    <BrowserRouter>
      <Routes>
          <Route path="login" element={<Login />} />
          <Route path="insta" element={<Insta />} />
          <Route path="drafts" element={<Drafts />} />
          <Route path="*" element={<Login />} />
          <Route path="instructor" element={<Instructor />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
