import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import React from 'react'
import { useNavigate,Link,createSearchParams } from 'react-router-dom';
import './Login.css';
import logo from './Logo.png';

const Login = () => {
    const navigate = useNavigate();
    const openinstaS=(id)=>{
        navigate({
            pathname:'/insta',
            search: createSearchParams({
                id:"student"
            }).toString()
        })

    }
    const openinstaT=(id)=>{
        navigate({
            pathname:'/insta',
            search: createSearchParams({
                id:"instructor"
            }).toString()
        })

    }
    return (
        <div className='logindiv'>
            <img className='loginlogo' src={logo} alt="React" />
            <div className='logincard'>

                <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" />
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicCheckbox">
                        {['radio'].map((type) => (
                            <div key={`inline-${type}`} className="mb-3">
                                <Form.Check
                                    inline
                                    label="Student"
                                    name="group1"
                                    type={type}
                                    id={`inline-${type}-1`}
                                />
                                <Form.Check
                                    inline
                                    label="Instructor/ TA"
                                    name="group1"
                                    type={type}
                                    id={`inline-${type}-2`}
                                />
                            </div>
                        ))}
                    </Form.Group>
                    <Button className='login' type="submit" onClick={() => { if (document.getElementById(`inline-radio-1`).checked == true) { return openinstaS() } else { return openinstaT() } }} >
                        Submit
                    </Button>
                </Form>
            </div >
        </div >

    );
}

export default Login;