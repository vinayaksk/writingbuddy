import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './Drafts.css';
import { GrammarlyEditorPlugin } from "@grammarly/editor-sdk-react";
import logo from './Logo.png';
import Dropdown from 'react-bootstrap/Dropdown';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import * as d3 from 'd3';
import { Component, createRef } from 'react';
import ReactWordcloud from 'react-wordcloud';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Login from './Login';

class Drafts extends Component {

    constructor(props) {
        super(props);
        this.state = {
            editorState: EditorState.createEmpty(),
            body: "",
            results: '',
            plag: {},
            sa: {},
            words: []
        };
        this.onEditorStateChange = editorState => this.setState({ editorState });

    }

    handleFormSubmit(e) {
        e.preventDefault();
        d3.select("svg").remove();
        document.getElementById('wordcloud').hidden = true
        document.getElementById('reptxt').hidden = false
        document.getElementById("reptxt").innerHTML = "Genrating";
        document.getElementById("rephd").innerHTML = "";

        this.state.body = this.removeTags(document.getElementById("esstxt").value)
        axios({
            method: 'post',
            url: "http://localhost:5000/send",
            headers: { 'content-type': 'application/json' },
            data: this.state
        })
            .then(result => {
                this.setState({ results: result.data }, this.runAnn);
            })
            .catch(error => {
                d3.select("svg").remove();
                document.getElementById('wordcloud').hidden = true
                document.getElementById('reptxt').hidden = false
                document.getElementById("reptxt").innerHTML = error + " (Please try again)";
                document.getElementById("rephd").innerHTML = "";
                console.log(error);
            });
    }

    removeTags(str) {
        if ((str === null) || (str === ''))
            return false;
        else
            str = str.toString();
        return str.replace(/(<([^>]+)>)/ig, '');
    }

    async runSA(e) {
        e.preventDefault();
        d3.select("svg").remove();
        document.getElementById('wordcloud').hidden = true
        document.getElementById('reptxt').hidden = false
        document.getElementById("rephd").innerHTML = "";
        document.getElementById("reptxt").innerHTML = "Generating";

        let axios = require('axios');
        const options = {
            method: 'GET',
            url: 'https://twinword-sentiment-analysis.p.rapidapi.com/analyze/',
            params: {
                text: this.removeTags(document.getElementById("esstxt").value)
            },
            headers: {
                'content-type': 'application/octet-stream',
                'X-RapidAPI-Key': 'c452c9a1c1mshff043e4d0bb55cfp18cb8bjsna7e30d6dd0c3',
                'X-RapidAPI-Host': 'twinword-sentiment-analysis.p.rapidapi.com'
            }
        };
        try {
            const response = await axios.request(options);
            this.setState({ sa: response.data }, this.showsa);
            console.log(response.data);
        } catch (error) {
            d3.select("svg").remove();
            document.getElementById('wordcloud').hidden = true
            document.getElementById('reptxt').hidden = false

            document.getElementById("reptxt").innerHTML = error + " (Please try again)";
            document.getElementById("rephd").innerHTML = "";
            console.error(error);
        }
    }

    showsa() {
        d3.select("svg").remove();
        document.getElementById('wordcloud').hidden = true
        document.getElementById('reptxt').hidden = false
        document.getElementById("rephd").innerHTML = "";
        document.getElementById("reptxt").innerHTML = "Genrating";
        var hd = this.state.sa['type'];

        this.state = this.setState({
            words: this.state.sa.keywords.map(item => ({
                text: item.word,
                value: item.score * 100
            }))
        })

        document.getElementById("rephd").innerHTML = hd.charAt(0).toUpperCase() + hd.slice(1)
        document.getElementById('reptxt').hidden = true
        document.getElementById('wordcloud').hidden = false
    }

    plagReport() {
        d3.select("svg").remove();
        document.getElementById('wordcloud').hidden = true
        document.getElementById('reptxt').hidden = false
        document.getElementById("reptxt").innerHTML = "Click pie segments for more information";
        document.getElementById("rephd").innerHTML = "";
        var samp = this.state.plag
        var data = [samp["uniquePercent"], samp['plagPercent']];
        var label = ["Unique", "Plagerised"]
        var svg = d3.select("div#report").append('svg').attr("width", 400)
            .attr("height", 150),
            width = 150,
            height = svg.attr("height"),
            radius = Math.min(width, height) / 2,
            g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var color = d3.scaleOrdinal(['#ED8B00', '#2CD5C4']);

        // Generate the pie
        var pie = d3.pie();

        // Generate the arcs
        var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(radius);

        //Generate groups
        var arcs = g.selectAll("arc")
            .data(pie(data))
            .enter()
            .append("g")
            .attr("class", "arc")
            .on("click", function (e, i) {
                document.getElementById("reptxt").innerHTML = "";
                document.getElementById("rephd").innerHTML = "";
                var pl = ['false', 'true']
                var fp = 0
                var uop = pl[i.index]
                if (samp['plagPercent'] == 100) fp = 1;
                if (samp['uniquePercent'] == 100) uop = pl[1];
                var lines = samp['details'].filter(element => element.unique == uop)
                var sources = samp["sources"]
                var txt = ""
                var hd = ""
                for (var x = 0; x < lines.length; x++) {
                    if (uop == 'false' || fp == 1) {
                        hd = "Plagerised"
                        txt += lines[x].query + ' ' + '(<a style="font-size: 10px" href="' + lines[x].display.url + '">' + lines[x].display.url + '</a> )<br>'
                    } else {
                        hd = "Unique"
                        txt += lines[x].query + '<br>'
                    }
                }
                document.getElementById("reptxt").innerHTML = txt;
                document.getElementById("rephd").innerHTML = hd
            }
            );

        //Draw arc paths
        arcs.append("path")
            .attr("fill", function (d, i) {
                return color(i);
            })
            .attr("d", arc)


        //Writte sect
        arcs.append("text")
            .attr("text-anchor", "middle")
            .attr("transform", function (d) { return "translate(" + arc.centroid(d) + ")"; })
            .text(function (d, i) { return data[i] + '%' });

        var legendRectSize = 18;
        var legendSpacing = 4;

        var legend = svg.selectAll('.legend')
            .data(label)
            .enter()
            .append('g')
            .attr('class', 'legend')
            .attr('transform', function (d, i) {
                var height = legendRectSize + legendSpacing;
                var horz = -2 * legendRectSize;
                var vert = i * height;
                return 'translate(' + horz + ',' + vert + ')';
            });
        legend.append("rect")
            .attr("x", 200)
            .attr("y", 9)
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", (d, i) => color(i));

        legend.append("text")
            .attr("x", 220)
            .attr("y", 18)
            .attr("dy", ".35em")
            .style("text-anchor", "start")
            .text(function (d) {
                return d.charAt(0).toUpperCase() + d.slice(1);
            });



    }
    runPlag(e) {
        e.preventDefault();
        d3.select("svg").remove();
        document.getElementById('wordcloud').hidden = true
        document.getElementById('reptxt').hidden = false
        document.getElementById("rephd").innerHTML = "";
        document.getElementById("reptxt").innerHTML = "Generating";
        const axios = require('axios');

        const data = new FormData();
        data.append('key', '310ab346d6c51b15ff7ddb7681fb91fe');
        data.append('data', this.removeTags(document.getElementById("esstxt").value));
        data.append('ignore', 'YOUR_MATCH_URL');

        axios.post('https://www.prepostseo.com/apis/checkPlag', data, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        })
            .then(response => {
                this.setState({ plag: response.data }, this.plagReport);
            })
            .catch(error => {
                d3.select("svg").remove();
                document.getElementById('wordcloud').hidden = true
                document.getElementById('reptxt').hidden = false

                document.getElementById("reptxt").innerHTML = error + " (Please try again)";
                document.getElementById("rephd").innerHTML = "";
                console.error(error);
            });


    }
    runAnn() {
        d3.select("svg").remove();
        document.getElementById('wordcloud').hidden = true
        document.getElementById('reptxt').hidden = false
        document.getElementById("reptxt").innerHTML = "Click pie segments for more information";
        document.getElementById("rephd").innerHTML = "";
        var data = this.state.results
        var labels = Array.from(new Set(data.map((x) => (x.class))))
        var count = []
        for (var x = 0; x < labels.length; x++) {
            count[x] = data.filter(element => element.class == labels[x]).length
        }
        var svg = d3.select("div#report").append('svg').attr("width", 400)
            .attr("height", 180),
            width = 150,
            height = 150,
            radius = Math.min(width, height) / 2,
            g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
        var color = d3.scaleOrdinal(['#642667', '#CE0058', '#ED8B00', '#2CD5C4', '#861F41', '#F7EA48', '#508590', '#D7D2CB']);
        var tcolor = d3.scaleOrdinal(['#FFFFFF', '#000000', '#000000', '#000000', '#FFFFFF', '#000000', '#000000', '#000000']);
        // Generate the pie
        var pie = d3.pie().sort(null);
        // Generate the arcs
        var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(radius);
        //Generate groups
        var arcs = g.selectAll("arc")
            .data(pie(count))
            .enter()
            .append("g")
            .attr("class", "arc")
            .on("click", function (e, i) {
                document.getElementById("reptxt").innerHTML = "";
                document.getElementById("rephd").innerHTML = "";
                console.log(i)
                var t = labels[i.index]
                var lines = data.filter(e => e.class == t).map((x) => x.text)
                var txt = "<br>"
                lines.forEach(element => { txt += element + "<br>" });
                document.getElementById("reptxt").innerHTML = txt
                document.getElementById("rephd").innerHTML = t
            });

        var txt = "Click pie segments for specific sentences. <br>"
        data.forEach(d => {
            var c = color(labels.indexOf(d.class))
            var tc = tcolor(labels.indexOf(d.class))
            txt += '<span id={d.class} style="background-color:' + c + '; color:' + tc + ';">' + d.text + '</span> '

        })
        document.getElementById("reptxt").innerHTML = txt
        //Draw arc paths
        arcs.append("path")
            .attr("fill", function (d, i) {
                return color(i);
            })
            .attr("d", arc);
        //Writte sect
        arcs.append("text")
            .attr("text-anchor", "middle")
            .attr("transform", function (d) { return "translate(" + arc.centroid(d) + ")"; })
            .text(function (d, i) { return d.data });

        var legendRectSize = 18;
        var legendSpacing = 4;

        svg.append("text")
            .attr("x", 150)
            .attr("y", 10)
            .attr("dy", ".35em")
            .style("text-anchor", "start")
            .text(function (d) {
                return "Breakup by # of sentences";
            });
        var legend = svg.selectAll('.legend')
            .data(labels)
            .enter()
            .append('g')
            .attr('class', 'legend')
            .attr('transform', function (d, i) {
                var height = legendRectSize + legendSpacing;
                var horz = -2 * legendRectSize;
                var vert = i * height + 20;
                return 'translate(' + horz + ',' + vert + ')';
            });
        legend.append("rect")
            .attr('id', (d) => d)
            .attr("x", 200)
            .attr("y", 9)
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", (d, i) => color(i));

        legend.append("text")
            .attr("x", 220)
            .attr("y", 18)
            .attr("dy", ".35em")
            .style("text-anchor", "start")
            .text(function (d) {
                return d;
            });

    }

    clrbtn() {
        d3.select("svg").remove();
        document.getElementById('wordcloud').hidden = true
        document.getElementById('reptxt').hidden = false
        document.getElementById("reptxt").innerHTML = "";
        document.getElementById("rephd").innerHTML = "";
    }

    render() {

        const callbacks = {
            getWordColor: word => word.value > 0 ? "blue" : "red",
            onWordClick: console.log,
            onWordMouseOver: console.log,
            getWordTooltip: word => `${word.text} (${word.value}) [${word.value > 0 ? "positive" : "negative"}]`,
        }
        const size = [150, 300];
        return (

            <div className="draftPage" >
                <div className="editor">
                    <GrammarlyEditorPlugin menuPosition="left" clientId="client_KjYDhiXhUR3aALKRnzCZQS">
                        <Editor
                            editorState={this.state.editorState} onEditorStateChange={this.onEditorStateChange}
                        />
                    </GrammarlyEditorPlugin>
                    <grammarly-button></grammarly-button>
                </div>
                <div className='editbtn'>

                    <Button className='plbtn' onClick={e => this.runPlag(e)} type="submit" >
                        Run Plagerism Checker
                    </Button>
                    <Button className='sabtn' onClick={e => this.runSA(e)} type="submit" >
                        Run Sentiment Analysis
                    </Button>
                    <p></p>
                    <form className="form-group">
                        <textarea id='esstxt'
                            disabled
                            hidden
                            value={
                                draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
                            }
                            onChange={e => this.setState({ body: e.target.value })}
                        />
                        <Button className='annbtn' onClick={
                            e => this.handleFormSubmit(e)
                        }  >
                            Run Annotator
                        </Button>
                    </form>

                    <div className='editop'></div>
                    <div className='opdiv'>
                        <p className='ap'>Analysis Report:</p>
                        <div id="report"></div>
                        <h2 id='rephd'></h2>
                        <div className='report' id="reptxt" ></div>
                        <div className='un' hidden id="wordcloud" ><ReactWordcloud
                            callbacks={callbacks}
                            size={size}
                            words={this.state.words} /></div>
                    </div>
                    <Button className='plbtn' onClick={this.clrbtn} type="submit" >
                        Clear
                    </Button>
                </div>

                <Link to="/insta?id=student"><img className="draftlogo" src={logo} alt="React" /></Link>


                <Dropdown className="draftdd">
                    <Dropdown.Toggle className="dd">
                        Menu
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item href="/login">Logout</Dropdown.Item>
                        <Dropdown.Item href="/insta?id=student">Drafts</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>



            </div >
        );
    }
}

export default Drafts;