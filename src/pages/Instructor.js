import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './Instructor.css';
import { GrammarlyEditorPlugin } from "@grammarly/editor-sdk-react";
import logo from './Logo.png';
import Dropdown from 'react-bootstrap/Dropdown';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import * as d3 from 'd3';
import { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class Instructor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editorState: EditorState.createEmpty(),
            body: "",
            results: '',
            plag: {},
        };
        this.onEditorStateChange = editorState => this.setState({ editorState });
    }
    handleFormSubmit(e) {
        e.preventDefault();
        d3.select("svg").remove();
        document.getElementById("reptxt").innerHTML = "Generating";
        document.getElementById("rephd").innerHTML = "";
        document.getElementById('reptxt').hidden = false

        this.state.body = this.removeTags(document.getElementById("esstxt").value)
        axios({
            method: 'post',
            url: "http://localhost:5000/ta_send",
            headers: { 'content-type': 'application/json' },
            data: this.state
        })
            .then(result => {
                this.setState({ results: result.data }, this.runAI);
            })
            .catch(error => {
                d3.select("svg").remove();
                document.getElementById("reptxt").innerHTML = error + " (Please try again)";
                document.getElementById("rephd").innerHTML = "";
                document.getElementById('reptxt').hidden = false

                console.log(error);
            });


    }
    removeTags(str) {
        if ((str === null) || (str === ''))
            return false;
        else
            str = str.toString();
        return str.replace(/(<([^>]+)>)/ig, '');
    }
    plagReport() {
        d3.select("svg").remove();
        document.getElementById("reptxt").innerHTML = "Click pie segments for more information";
        document.getElementById("rephd").innerHTML = "";
        document.getElementById('reptxt').hidden = false
        var samp = this.state.plag

        //require('./sample.json')
        var data = [samp["uniquePercent"], samp['plagPercent']];
        var label = ["Unique", "Plagerised"]
        var svg = d3.select("div#report").append('svg').attr("width", 400)
            .attr("height", 150),
            width = 150,
            height = svg.attr("height"),
            radius = Math.min(width, height) / 2,
            g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var color = d3.scaleOrdinal(['#ED8B00', '#2CD5C4']);

        // Generate the pie
        var pie = d3.pie();

        // Generate the arcs
        var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(radius);

        //Generate groups
        var arcs = g.selectAll("arc")
            .data(pie(data))
            .enter()
            .append("g")
            .attr("class", "arc")
            .on("click", function (e, i) {
                document.getElementById("reptxt").innerHTML = "";
                document.getElementById("rephd").innerHTML = "";
                var pl = ['false', 'true']
                var fp = 0
                var uop = pl[i.index]
                if (samp['plagPercent'] == 100) fp = 1;
                if (samp['uniquePercent'] == 100) uop = pl[1];
                var lines = samp['details'].filter(element => element.unique == uop)
                var sources = samp["sources"]
                var txt = ""
                var hd = ""
                for (var x = 0; x < lines.length; x++) {
                    if (uop == 'false' || fp == 1) {
                        hd = "Plagerised"
                        txt += lines[x].query + ' ' + '(<a style="font-size: 10px" href="' + lines[x].display.url + '">' + lines[x].display.url + '</a> )<br>'
                    } else {
                        hd = "Unique"
                        txt += lines[x].query + '<br>'
                    }
                }
                document.getElementById("reptxt").innerHTML = txt;
                document.getElementById("rephd").innerHTML = hd
            }
            );

        //Draw arc paths
        arcs.append("path")
            .attr("fill", function (d, i) {
                return color(i);
            })
            .attr("d", arc)


        //Writte sect
        arcs.append("text")
            .attr("text-anchor", "middle")
            .attr("transform", function (d) { return "translate(" + arc.centroid(d) + ")"; })
            .text(function (d, i) { return data[i] + '%' });

        var legendRectSize = 18;
        var legendSpacing = 4;

        var legend = svg.selectAll('.legend')
            .data(label)
            .enter()
            .append('g')
            .attr('class', 'legend')
            .attr('transform', function (d, i) {
                var height = legendRectSize + legendSpacing;
                var horz = -2 * legendRectSize;
                var vert = i * height;
                return 'translate(' + horz + ',' + vert + ')';
            });
        legend.append("rect")
            .attr("x", 200)
            .attr("y", 9)
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", (d, i) => color(i));

        legend.append("text")
            .attr("x", 220)
            .attr("y", 18)
            .attr("dy", ".35em")
            .style("text-anchor", "start")
            .text(function (d) {
                return d.charAt(0).toUpperCase() + d.slice(1);
            });



    }
    runPlag(e) {
        e.preventDefault();
        d3.select("svg").remove();
        document.getElementById("rephd").innerHTML = "";
        document.getElementById("reptxt").innerHTML = "Generating";
        const axios = require('axios');

        const data = new FormData();
        data.append('key', '310ab346d6c51b15ff7ddb7681fb91fe');
        data.append('data', this.removeTags(document.getElementById("esstxt").value));
        data.append('ignore', 'YOUR_MATCH_URL');

        axios.post('https://www.prepostseo.com/apis/checkPlag', data, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        })
            .then(response => {
                d3.select("svg").remove();
                document.getElementById("reptxt").innerHTML = "Genrating";
                document.getElementById("rephd").innerHTML = "";
                document.getElementById('reptxt').hidden = false
                this.setState({ plag: response.data }, this.plagReport);

            })
            .catch(error => {
                d3.select("svg").remove();
                document.getElementById("reptxt").innerHTML = error + " (Please try again)";
                document.getElementById("rephd").innerHTML = "";
                document.getElementById('reptxt').hidden = false
                console.error(error);
            });


    }
    runAI() {
        d3.select("svg").remove();
        document.getElementById("reptxt").innerHTML = "Genrating";
        document.getElementById("rephd").innerHTML = "";
        document.getElementById('reptxt').hidden = false
        var tags = { 'very unlikely': 'Very Unlikely', "unlikely": 'Unlikely', "unclear if it is": 'Unclear', "possibly": 'Possibly', "likely": 'Likely' }
        var data = this.state.results
        //{ 'Class': 'likely', 'AI-Generated Probability': 99.70968134204543 }
        //this.state.results
        console.log(data)
        var svg = d3.select("div#report").append('svg').attr("width", 400)
            .attr("height", 200),
            width = 200,
            height = svg.attr("height"),
            radius = Math.min(width, height) / 2,
            g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
        var p = Math.floor(data['AI-Generated Probability'])
        var pie_data = [p, 100 - p]
        var color = d3.scaleOrdinal(['#642667', 'rgb(100, 38, 103,0.2)']);
        // Generate the pie
        var pie = d3.pie().sort(null);
        // Generate the arcs
        var arc = d3.arc()
            .innerRadius(radius - 30)
            .outerRadius(radius);
        //Generate groups
        var arcs = g.selectAll("arc")
            .data(pie(pie_data))
            .enter()
            .append("g")
            .attr("class", "arc")
        //Draw arc paths
        arcs.append("path")
            .attr("fill", function (d, i) {
                return color(i);
            })
            .attr("d", arc);
        g.append("svg:text")
            .attr("dy", "-.25em")
            .attr("text-anchor", "middle")
            .attr("font-size", "20")
            .text(data['AI-Generated Probability'].toFixed(4) + "%");
        g.append("svg:text")
            .attr("dy", "1.35em")
            .attr("text-anchor", "middle")
            .attr("font-size", "20")
            .text("Confidence");
        g.append("svg:text")
            .attr("x", 200)
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .attr("font-size", "30")
            .attr("font-family", "Impact,Charcoal,sans-serif")
            .text(tags[data.Class]);
        
        var c = data.Class
        var pr = String(data['AI-Generated Probability'].toFixed(4))
        document.getElementById('reptxt').hidden = true
        document.getElementById("rephd").innerHTML = "<br>The model classifies the text to be " + c + " AI-generated. <br> The model's confidence score is " + pr + '%.'
    }
    clrbtn() {
        d3.select("svg").remove();
        document.getElementById("reptxt").innerHTML = "";
        document.getElementById("rephd").innerHTML = "";
        document.getElementById('reptxt').hidden = false
    }
    render() {
        
        return (

            <div className="draftPage" >
                <div className="editor">
                    <Editor
                        editorState={this.state.editorState}
                        onEditorStateChange={this.onEditorStateChange}
                        toolbar={{
                            options: ['history'],
                            history: { inDropdown: true }
                        }}
                    />
                </div>
                <div className='editbtn'>
                    <form className="form-group">
                        <textarea id='esstxt'
                            disabled
                            hidden
                            value={
                                draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))
                            }
                            onChange={e => this.setState({ body: e.target.value })}
                        />
                        <Button className='annbtn' onClick={
                            e => this.handleFormSubmit(e)
                        }  >
                            Run AI Checker
                        </Button>
                    </form>
                    <p></p>
                    <Button className='plbtn' onClick={e => this.runPlag(e)} type="submit" >
                        Run Plagerism Checker
                    </Button>

                    <div className='editop'></div>
                    <div className='opdiv'>
                        <p className='ap'>Analysis Report:</p>
                        <div id="report"></div>
                        <h3 id='rephd'></h3>
                        <div className='report' id="reptxt" ></div>
                    </div>
                    <Button className='plbtn' onClick={this.clrbtn} type="submit" >
                        Clear
                    </Button>
                </div>
                <Link to="/insta?id=instructor"><img className="draftlogo" src={logo} alt="React" /></Link>
                <Dropdown className="draftdd">
                    <Dropdown.Toggle className="dd">
                        Menu
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item href="/login">Logout</Dropdown.Item>
                        <Dropdown.Item href="/insta?id=instructor">Back</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>



            </div >
        );
    }
}
export default Instructor;