import React from 'react'
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileArrowUp } from '@fortawesome/free-solid-svg-icons'
import "./Insta.css";
import 'font-awesome/css/font-awesome.min.css';
import logo from './logowhite.png';
import upload from './upload.png';
import type from './type.png';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import { useNavigate, useSearchParams } from 'react-router-dom';

function Insta() {
    const [searchParams] = useSearchParams();
    var txt = ""
    if (searchParams.get("id") == 'instructor') {
        txt = "Upload Files"
    } else {
        txt = "Upload File"
    }

    const history = useNavigate();
    return (
        <div className='secs'>

            <div className='sec1'>
                <Button className='fileup1' type="submit" >
                    {txt}  <FontAwesomeIcon icon={faFileArrowUp} />
                    <input className='form-control' id='fileup' type="file" />
                </Button>
                <img className='iconslogo1' src={upload} alt="React" />
                <Link to="/login"><img className='instalogo' src={logo} alt="React" /></Link>
            </div>

            <div className='sec2'>
                <Button className='fileup2' type="submit" onClick={() => { if (searchParams.get("id") == 'instructor') { return history('/Instructor') } else { return history('/drafts') } }} >
                    Input Text
                </Button>
                <img className='iconslogo2' src={type} alt="React" />
            </div>
        </div>
    );
}
export default Insta;