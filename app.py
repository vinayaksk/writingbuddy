from flask import Flask, request, jsonify
from flask_cors import CORS
import model
from detector import OpenaiDetector

app = Flask(__name__)
CORS(app)

@app.route("/send", methods=["GET", "POST"])
def send():
    if request.method == "POST":
        data = str(request.json["body"])
        # print(data)
        output = model.generate_annotations(data).to_json(orient="records")
        print(output)
        return output

@app.route("/ta_send", methods=["GET", "POST"])
def ta_send():
    if request.method == "POST":
        data = str(request.json["body"])
        # print(data)
        bearer_token = 'Bearer sess-w6x6Jw8xHivhr9LwznLflM4eQvov51epubLUPTn4'
        od = OpenaiDetector(bearer_token)
        response = od.detect(data)
        print(response)
        return response

if __name__ == '__main__':
    app.run()