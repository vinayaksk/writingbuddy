import transformers
import nltk
from datasets import Dataset
import pandas as pd
import os
import uuid

nltk.download('punkt')
tokenizer = transformers.AutoTokenizer.from_pretrained("input/feedback-bert-trained/")

data_collator = transformers.DataCollatorForTokenClassification(tokenizer)

model = transformers.AutoModelForSequenceClassification.from_pretrained("input/feedback-bert-trained/",
                                                                        num_labels=8)

TEST_PATH = 'input/testing/'

trainer = transformers.Trainer(
    model,
    data_collator=data_collator,
    tokenizer=tokenizer,
)


# data="Neural networks have become increasingly popular in recent years and have been applied to a wide range of tasks such as image recognition, natural language processing, and self-driving cars. They have proven to be powerful tools for solving complex problems and have enabled significant advancements in technology. However, the question remains: are neural networks the future of computing and machine learning?" \
#      "There are several reasons why neural networks are likely to be a significant part of the future of computing. One of the key strengths of neural networks is their ability to learn from data and generalize their understanding to new situations. This makes them a powerful tool for a wide range of applications, and their ability to learn and adapt has the potential to revolutionize many industries." \
#      "Another reason why neural networks are likely to be a significant part of the future is their ability to process large amounts of data quickly and efficiently. As more and more data becomes available, the ability to analyze and make sense of that data becomes increasingly important. Neural networks can process data in real-time, making them well-suited for tasks such as self-driving cars, robotics, and other systems that require quick and real-time processing." \
#      "Furthermore, the development of new architectures and algorithms is continually pushing the boundaries of what neural networks can do. Researchers are constantly working to improve the accuracy, speed, and efficiency of neural networks, and new breakthroughs are being made all the time. " \
#      "However, there are also challenges and limitations to the use of neural networks. One of the challenges is the need for large amounts of data to train the networks. This can be a limitation for applications where data is limited or where the cost of collecting and labeling data is high. " \
#      "Another challenge is the need for significant computational resources. Neural networks can be computationally intensive, requiring specialized hardware and software to run efficiently. This can be a limitation for smaller organizations or those without access to high-performance computing resources. " \
#      "Finally, there are concerns about the interpretability and transparency of neural networks. Because neural networks are highly complex, it can be difficult to understand how they arrive at their decisions. This can be a limitation in applications such as healthcare or finance, where decisions must be explainable and transparent. I" \
#      "In conclusion, while there are challenges and limitations to the use of neural networks, their ability to learn from data and generalize to new situations, process large amounts of data quickly and efficiently, and the development of new architectures and algorithms are likely to make them a significant part of the future of computing and machine learning. However, continued research and development will be necessary to overcome the challenges and limitations and to ensure that neural networks are used in a responsible and transparent way."
def preprocess_function(examples):
    return tokenizer(examples["text"], truncation=True, max_length=326)

def get_test_text(a_id):
    a_file = f"{TEST_PATH}/{a_id}.txt"
    with open(a_file, "r") as fp:
        txt = fp.read()
    return txt


def create_df_test(text_data):
    test_ids = [str(uuid.uuid1())]
    test_data = []
    for test_id in test_ids:
        # text = get_test_text(test_id)
        text = text_data
        sentences = nltk.sent_tokenize(text)
        id_sentences = []
        idx = 0
        for sentence in sentences:
            id_sentence = []
            words = sentence.split()
            # I created this heuristic for mapping words in senteces to "word indexes"
            # This is not definitive and might have strong drawbacks and problems
            for w in words:
                id_sentence.append(idx)
                idx += 1
            id_sentences.append(id_sentence)
        test_data += list(zip([test_id] * len(sentences), sentences, id_sentences))
    df_test = pd.DataFrame(test_data, columns=['id', 'text', 'ids'])
    return df_test

def generate_annotations(data):

    df_test = create_df_test(data)

    ds_test = Dataset.from_pandas(df_test)
    ds_test_tokenized = ds_test.map(preprocess_function, batched=True)

    test_predictions = trainer.predict(ds_test_tokenized)

    # Turn logits into classes
    df_test['predictions'] = test_predictions.predictions.argmax(axis=1)

    ID2CLASS={0: 'Lead', 1: 'Position', 2: 'Evidence', 3: 'Claim', 4: 'Concluding Statement', 5: 'Counterclaim',
          6: 'Rebuttal', 7: 'No Class'}
    # Turn class ids into class labels
    df_test['class'] = df_test['predictions'].map(ID2CLASS)
    return df_test[["text","class"]]

# Test o/p
# data="Neural networks have become increasingly popular in recent years and have been applied to a wide range of tasks such as image recognition, natural language processing, and self-driving cars. They have proven to be powerful tools for solving complex problems and have enabled significant advancements in technology. However, the question remains: are neural networks the future of computing and machine learning?" \
#      "There are several reasons why neural networks are likely to be a significant part of the future of computing. One of the key strengths of neural networks is their ability to learn from data and generalize their understanding to new situations. This makes them a powerful tool for a wide range of applications, and their ability to learn and adapt has the potential to revolutionize many industries." \
#      "Another reason why neural networks are likely to be a significant part of the future is their ability to process large amounts of data quickly and efficiently. As more and more data becomes available, the ability to analyze and make sense of that data becomes increasingly important. Neural networks can process data in real-time, making them well-suited for tasks such as self-driving cars, robotics, and other systems that require quick and real-time processing." \
#      "Furthermore, the development of new architectures and algorithms is continually pushing the boundaries of what neural networks can do. Researchers are constantly working to improve the accuracy, speed, and efficiency of neural networks, and new breakthroughs are being made all the time." \
#      "However, there are also challenges and limitations to the use of neural networks. One of the challenges is the need for large amounts of data to train the networks. This can be a limitation for applications where data is limited or where the cost of collecting and labeling data is high." \
#      "Another challenge is the need for significant computational resources. Neural networks can be computationally intensive, requiring specialized hardware and software to run efficiently. This can be a limitation for smaller organizations or those without access to high-performance computing resources." \
#      "Finally, there are concerns about the interpretability and transparency of neural networks. Because neural networks are highly complex, it can be difficult to understand how they arrive at their decisions. This can be a limitation in applications such as healthcare or finance, where decisions must be explainable and transparent." \
#      "In conclusion, while there are challenges and limitations to the use of neural networks, their ability to learn from data and generalize to new situations, process large amounts of data quickly and efficiently, and the development of new architectures and algorithms are likely to make them a significant part of the future of computing and machine learning. However, continued research and development will be necessary to overcome the challenges and limitations and to ensure that neural networks are used in a responsible and transparent way."
# print(generate_annotations(data).to_json(orient="records"))